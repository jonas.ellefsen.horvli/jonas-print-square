﻿using System;

namespace PrintSquare
{
    class Program
    {
        static void Main(string[] args)
        {
            int squareWidth = HelperFunctions.requestNumber("square width");
            int squareHeight = HelperFunctions.requestNumber("square height");
            HelperFunctions.printSquare(squareHeight, squareWidth);
           
        }
    }

    class HelperFunctions
    {
        public static int requestNumber(string name)
        {
            int number = 0;
            Console.WriteLine($"Enter {name}:");
            while (!Int32.TryParse(Console.ReadLine(), out number) || number <= 0)
            {
                Console.WriteLine("You must enter a positive number.");
            }
            return number;
        }
        public static void printSquare(int squareHeight, int squareWidth)
        {
            for (int y = 0; y < squareHeight; y++)
            {
                for (int x = 0; x < squareWidth; x++)
                {
                    if (x == 0 || x == squareWidth - 1 || y == 0 || y == squareHeight - 1)
                    {
                        Console.Write('#');
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }
                Console.Write('\n');
            }
        }
    }
}
